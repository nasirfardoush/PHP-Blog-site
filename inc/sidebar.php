<div class="sidebar clear">
	<div class="samesidebar clear">
		<h2>Categories</h2>
			<ul>
			<?php 
				$query = "SELECT * FROM tbl_category";
				$category= $db->select($query);
				if($category){
					while ( $result = $category->fetch_assoc()) { ?>
						<li><a href="category.php?catID=<?php echo $result['id'];?>"><?php echo $result['category'];?></a></li>
				<?php } }else{ ?>
					<li>No category found</li>
					<?php } ?>
									
			</ul>
	</div>
	
	<div class="samesidebar clear">
		<h2>Latest articles</h2>
			<?php 
				$query = "SELECT * FROM tbl_post ORDER BY ID DESC LIMIT 5";
				$category= $db->select($query);
				if($category){
					while ( $result = $category->fetch_assoc()) { ?>

						<div class="popular clear">
							<h3><a href="single.php?postID=<?php echo $result['id'];?>"><?php echo $result['title'];?></a></h3>
							<a href="single.php?postID=<?php echo $result['id'];?>"><img src="admin/<?php echo $result['img'];?>" alt="post image"/></a>
							<?php echo $fm->readmore($result['body'],20);?>
						</div>

				<?php } }else{
					header('Locatoin: 404.php');
				} ?>

	</div>
	
</div>