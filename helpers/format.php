<?php 

class Format{
	public function formatDate($date) {
		return date('F j-Y g:i A',strtotime($date));
	}

	public function readmore ($text,$num) {
		$readmore = explode(" ", $text);
		$excerpt = array_slice($readmore,0,$num);
		return implode(" ", $excerpt);
	}	

	public function validation ($data) {
		$data = trim($data);
		$data = stripcslashes($data);   
		$data = htmlspecialchars($data);

		return $data;
	}

	public function title(){
		$path = $_SERVER['SCRIPT_FILENAME'];
		$title = basename($path,'.php');
		//$title = str_replace('_', ' ', $title); remove underscore $ use ucwords()
		if ($title == 'index') {
			$title = 'home';
		}elseif ($title == 'contact') {
			$title = 'contact';
		}
		return $title = ucfirst($title); //for capitaliz word // alternative use ucwords()



	}


}