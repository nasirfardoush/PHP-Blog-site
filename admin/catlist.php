﻿<?php
 include "inc/header.php";

 if (isset($_GET['cat_deleteID'])) {
 		$catDelID = $_GET['cat_deleteID'];
 $deldata = "DELETE FROM tbl_category WHERE id = '$catDelID' ";
 $Deldata    = $db->delete($deldata);
 if ($Deldata) {
  	$delmsg = "<p style='color:green'> Data deleted succesfully</p>";
 }else{
 	$delmsg = "<p style='color:green'> Data not deleted !</p>";
 }
}
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Category List</h2>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Category Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php if(isset($delmsg)){echo $delmsg;}?>

					<?php 
					$query = "SELECT * FROM tbl_category" ;
					$showData = $db->select($query);
					if ($showData) {
						$i = 0;
						while($result = $showData->fetch_assoc()){
							$i++; 	?>

						<tr class="odd gradeX">
							<td><?php echo $i;?></td>
							<td><?php echo $result['category'];?></td>
							<td><a href="catedit.php?cat_editID=<?php echo $result['id'];?>">Edit</a> || <a href="?cat_deleteID=<?php echo $result['id'];?>" onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
						</tr>
					<?php } } ?>	
					</tbody>
				</table>
               </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
 <?php
 include "inc/footer.php";
?>
