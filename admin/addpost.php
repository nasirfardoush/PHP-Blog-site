<?php
    include "inc/header.php";
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Post</h2>
                <div class="block">  
<?php 


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $title      = mysqli_real_escape_string ($db->link, $_POST['title']);
    $cat        = mysqli_real_escape_string($db->link, $_POST['cat']);
    $body       = mysqli_real_escape_string($db->link, $_POST['body']);
    $author     = mysqli_real_escape_string($db->link, $_POST['author']);
    $tag        = mysqli_real_escape_string($db->link, $_POST['tag']);

    $alow    = array('jpg','jpeg','png','gif');
    $file_name      = $_FILES['img']['name'];
    $file_size      = $_FILES['img']['size'];
    $file_path      = $_FILES['img']['tmp_name'];


    $explodeEX      = explode('.', $file_name);
    $unique_image   = strtolower(end($explodeEX));
    $file_Extention = substr(md5(time()), 0,7).'.'.$unique_image;

    $file_with_folder ="upload/".$file_Extention;
   

    if($title== " " || $cat== "" || $body=="" || $file_name == "") {
        echo "Filed can't be empty";
    }elseif ($file_size >1048576) {
        echo "file can't be large 1 MB";
    }elseif (in_array($unique_image , $alow )===false) {
        echo "You can upload only".implode(", ", $alow);
    }else{
        move_uploaded_file($file_path , $file_with_folder);
        $query = "INSERT INTO tbl_post (title,cat,body,img,author,tag) VALUES('$title','$cat','$body','$file_with_folder','$author','$tag')";
        $insert_data = $db->insert($query);
        if ($insert_data) {
            echo "Data inserted succesfully";
        }else{
            echo "Data not inserted";
        }
    }

}
?>

                 <form action="" method="POST" enctype="multipart/form-data">
                    <table class="form">                       
                        <tr>
                            <td>
                                <label>Title</label>
                            </td>
                            <td>
                                <input type="text" name="title" placeholder="Enter Post Title..." class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Category</label>
                            </td>
                            <td>
                                <select id="select" name="cat">
                <?php 
                    $query = "SELECT * FROM tbl_category";
                    $category  =  $db->select($query);
                    if($category){
                        while ($result = $category->fetch_assoc() ) { ?>
                            <option value="<?php echo $result['id'];?>"><?php echo $result['category'];?></option>
                    <?php }} ?>
                                    
                                </select>
                            </td>
                        </tr>
                   
                    
                        <tr>
                            <td>
                                <label>Upload Image</label>
                            </td>
                            <td>
                                <input type="file" name="img" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Content</label>
                            </td>
                            <td>
                                <textarea class="tinymce" name="body"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Author</label>
                            </td>
                          <td>
                            <input type="text" name="author" placeholder="Enter author name..." class="medium" />
                        </td>
                        </tr>
                         <tr>
                            <td>
                                <label>Tag</label>
                            </td>
                          <td>
                            <input type="text" name="tag" placeholder="Enter author name..." class="medium" />
                        </td>
                        </tr>
						<tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php
        include "inc/footer.php";
    ?>
