﻿<?php
 include "inc/header.php";

//Update Title Slogan and Logo
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $title  = $_POST['title'];
    $slogan = $_POST['slogan'];

    $allow_Ex  = array('png');
    $logo_name = $_FILES['logo']['name'];
    $logo_size = $_FILES['logo']['size'];
    $temp_path = $_FILES['logo']['tmp_name'];

    $explode         = explode('.',$logo_name);
    $reciveEx        = strtolower(end($explode));
    $logo_fixed_name = 'logo'.'.'.$reciveEx;

    $folder_with_logo ="upload/".$logo_fixed_name;
    move_uploaded_file($temp_path, $folder_with_logo);

     if (empty($title)) {
         $T_error = "<p style='color:red'> Title can't be empty !</p>";
     }elseif (empty($slogan)) {
        $S_error = "<p style='color:red'> Slogan can't be empty !</p>";
     }elseif (empty($logo_name)) {

       $query = "UPDATE title_logo SET 
                    title  = '$title',
                    slogan = '$slogan' WHERE id = '1' ";

            $updateData = $db->update($query);
            if ($updateData) {
                $msg = "<p style='color:green'> Data Updated Succesfully with before logo .</p>";
            }else{
                $msg = "<p style='color:red'> Data Not Updated !</p>";
            }

    }else{
        
        if ($logo_size > 102400) {
            $logo_error = "<p style='color:orange'> Logo size must be under 100KB !</p>";
        }elseif (in_array($reciveEx, $allow_Ex) === false) {
            $logo_error = "<p style='color:orange'> You can upload only png formate logo image.</p>";
        }else{
            $query = "UPDATE title_logo SET 
                    logo   = '$folder_with_logo',
                    title  = '$title',
                    slogan = '$slogan' WHERE id = '1' ";

            $updateData = $db->update($query);
            if ($updateData) {
                $msg = "<p style='color:green'> Data Updated Succesfully.</p>";
            }else{
                $msg = "<p style='color:red'> Data Not Updated !</p>";
            }
        }
        }
    }

    //Show title slogan Data for Edit
    $query = "SELECT * FROM title_logo WHERE id = '1' ";
    $showdata = $db->select($query);
    if ($showdata) {
        $result = $showdata->fetch_assoc();
    }


?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Update Site Title and Description</h2>
                <div class="block sloginblock">

                 <form action="titleslogan.php" method="POST" enctype="multipart/form-data">
                    <table class="form">
                        <?php if (isset($msg)) {
                            echo $msg;
                        } ?>					
                        <tr>
                            <td>
                                 <label>Website Title</label>
                            </td>
                            <td>
                                <?php if (isset($T_error)) {
                                echo $T_error;
                                } ?> 
                                <input type="text" value="<?php echo $result['title'];?>"  name="title" class="medium" />
                            </td>
                        </tr>
						 <tr>
                            <td>
                                <label>Website Slogan</label>
                            </td>
                            <td>
                                <?php if (isset($S_error)) {
                                echo $S_error;
                                } ?>
                                <input type="text" value="<?php echo $result['slogan'];?>" name="slogan" class="medium" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <label>Logo image</label>
                            </td>
                            <td>
                                <?php if (isset($logo_error)) {
                                    echo $logo_error;
                                } ?>
                                <input type="file"  name="logo" class="medium" />
                            </td>
                        </tr>
						 <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
<?php
 include "inc/footer.php";
?>