<?php
    include "inc/header.php";

    if(isset($_GET['replayID'])){
    $replayID = $_GET['replayID'];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $toemail      = $fm->validation($_POST['toemail']);
    $fromemail    = $fm->validation($_POST['fromemail']);
    $subject      = $fm->validation($_POST['subject']);
    $message      = $fm->validation($_POST['message']);

    $toemail_ok    = mysqli_real_escape_string($db->link, $toemail);
    $fromemail_ok  = mysqli_real_escape_string($db->link, $fromemail);
    $subject_ok    = mysqli_real_escape_string($db->link, $subject);
    $message_ok    = mysqli_real_escape_string($db->link, $message);

    $sendmail = mail($toemail_ok, $subject_ok, $message_ok, $fromemail_ok);
}
?>

        <div class="grid_10">
            <div class="box round first grid">
                <h2>Message Reaplay</h2>
                <div class="block">  
                 <form action="" method="POST">
                    <table class="form">                       
                        <tr>
                            <td>
                                <label>To</label>
                            </td>
                            <?php
                            if (isset($replayID)) {
                                $query = "SELECT * FROM tbl_contact WHERE id ='$replayID' ";
                                $showData = $db->select($query);
                                if ($showData) {
                                    while ($result = $showData->fetch_assoc()) {?>

                            <td>
                                <input type="email" readonly="" name="toemail" value="<?php echo $result['email'];?>" class="medium" />
                            </td>
                        <?php } } } ?>
                        </tr>
                        <tr>
                            <td>
                                <label>From</label>
                            </td>
                            <td>
                                <input type="email" name="fromemail" placeholder="Enter Post Title..." class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Subject</label>
                            </td>
                            <td>
                                <input type="text" name="subject" placeholder="Enter subject..." class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Message</label>
                            </td>
                            <td>
                                <textarea class="tinymce" name="message"></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="SEND" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php
        include "inc/footer.php";
    ?>
