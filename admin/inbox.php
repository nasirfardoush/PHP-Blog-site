﻿<?php
 include "inc/header.php";

 //message transfer to seen item
 if (isset($_GET['seenID'])) {
 	$seenID = $_GET['seenID'];
 	if (isset($seenID)) {
 			$query = "UPDATE tbl_contact SET status = '1' WHERE id = '$seenID'";
 			$updatedeata = $db->update($query);
 			if ($updatedeata) {
 				$msg = "<p style='color:green'> Message succesfully move to seen items.</p>";
 			}else{
 				$msg = "<p style='color:red'> Message not move to seen items.</p>";
 			}
 		}
 	}
 	//message delete from seen items
	 if (isset($_GET['msgDelID'])) {
	$msgDelID = $_GET['msgDelID'];
	if (isset($msgDelID)) {
			$data = "DELETE FROM  tbl_contact  WHERE id = '$msgDelID'";
			$deldeata = $db->update($data);
			if ($deldeata) {
				$del_msg = "<p style='color:green'> Message succesfully deleted.</p>";
			}else{
				$del_msg = "<p style='color:red'> Message not deleted.</p>";
			}
		}
	}
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Inbox</h2>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Message</th>
							<th>DateTime</th>
							<th>Action</th>
						</tr>
					</thead>
					<?php
						$query = "SELECT * FROM tbl_contact WHERE status = '0' ORDER BY ID DESC ";
						$showdata = $db->select($query);
						if ($showdata){
							$i= '0';
							while($result = $showdata->fetch_assoc()){$i++;?>
					<tbody>
						<tr class="odd gradeX">
							<td><?php echo $i.'.';?></td>
							<td><?php echo $result['first_name'].' '.$result['last_name'];?></td>
							<td><?php echo $result['email'];?></td>
							<td><?php echo $fm->readmore($result['message'],7);?></td>
							<td><?php echo $fm->formatDate($result['dateTime']);?></td>

							<td><a href="msgeView.php?msgViewID=<?php echo $result['id'];?>">View</a> || <a href="msgreplay.php?replayID=<?php echo $result['id'];?>">Replay</a> || <a href="?seenID=<?php echo $result['id'];?>">Seen</a>

							</td>
						</tr>
					</tbody>
					<?php } }else{
							echo "<p style='color:blue'>Inbox is empty !</p>";
							} ?>
					<?php if(isset($msg)){echo $msg;} ?>
				</table>
               </div>
            </div>
               	<div class="box round first grid">
                	<h2>Seen items</h2>
                   <div class="block">
                   <?php if (isset($del_msg)) {echo $del_msg; } ?>        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Message</th>
							<th>DateTime</th>
							<th>Action</th>
						</tr>
					</thead>
					<?php
						$query = "SELECT * FROM tbl_contact  WHERE status = '1' ORDER BY ID DESC ";
						$showdata = $db->select($query);
						if ($showdata){
							$i= '0';
							while($result = $showdata->fetch_assoc()){$i++;?>
					<tbody>
						<tr class="odd gradeX">
							<td><?php echo $i.'.';?></td>
							<td><?php echo $result['first_name'].' '.$result['last_name'];?></td>
							<td><?php echo $result['email'];?></td>
							<td><?php echo $fm->readmore($result['message'],7);?></td>
							<td><?php echo $fm->formatDate($result['dateTime']);?></td>

							<td><a href="?msgViewID=<?php echo $result['id'];?>">View</a> || <a href="msgreplay.php?replayID=<?php echo $result['id'];?>">Replay</a> || <a onclick="return confrim('Are you sure to delete this ?')" href="inbox.php?msgDelID=<?php echo $result['id'];?>">Delete</a></td>
						</tr>
					</tbody>
					<?php } }else{
							echo "<p style='color:blue'>seen item are empty !</p>";
							} ?>
				</table>
               </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
<?php
 include "inc/footer.php";
?>
