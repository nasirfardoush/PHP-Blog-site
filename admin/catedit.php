<?php
 include "inc/header.php";

 //Update data to category table

if (isset($_GET['cat_editID'])) {
    $editID = $_GET['cat_editID'];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
     $category = $_POST['category'];

     if (empty($category)) {
         $Ca_error = "<p style='color:orange'> Filed must not be empty !</p>";
     }else{
         if (isset($editID)) {
             $query = "UPDATE tbl_category SET category = '$category' WHERE id = '$editID' ";
                $update_row = $db->update($query);
                if ($update_row) {
                    $update_msg = "<p style='color:green'> Data Updated succesfully.</p>";
                }else{
                    $update_msg = "<p style='color:orange'> Data not Updated !</p>";
                }
         }
     }
}

//Show data category edit page
$data = "SELECT * FROM tbl_category WHERE id = '$editID' ";
$showdata = $db->select($data);
if ($showdata) {
    $result = $showdata->fetch_assoc();
}


?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Add New Category</h2>
               <div class="block copyblock"> 
                 <form action=" " method="POST">
                    <table class="form">					
                        <tr>
                            <td>
                                <?php if (isset($Ca_error)) {echo $Ca_error;}elseif(isset($update_msg)){echo $update_msg;
                                }?>

                                <input type="text" name="category" value="<?php echo $result['category'];?>" class="medium" />
                            </td>
                        </tr>
						<tr> 
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
   <?php include "inc/footer.php";?>
