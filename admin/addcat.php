<?php
 include "inc/header.php";

 //Add data to category table
 if ($_SERVER['REQUEST_METHOD'] == 'POST') {
     $category = $_POST['category'];

     if (empty($category)) {
         $Ca_error = "<p style='color:orange'> Filed must not be empty !</p>";
     }else{
        $query = "INSERT INTO tbl_category (category) VALUES('$category')";
        $insert_row = $db->insert($query);
        if ($insert_row) {
            $insert_msg = "<p style='color:green'> Data inserted succesfully.</p>";
        }else{
            $insert_msg = "<p style='color:orange'> Data not inserted !</p>";
        }
     }
 }
?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Add New Category</h2>
               <div class="block copyblock"> 
                 <form action="" method="POST">
                    <table class="form">					
                        <tr>
                            <td>
                                <?php if (isset($Ca_error)) {echo $Ca_error;}elseif(isset($insert_msg)){echo $insert_msg;
                                }?>

                                <input type="text" name="category" placeholder="Enter Category Name..." class="medium" />
                            </td>
                        </tr>
						<tr> 
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
   <?php include "inc/footer.php";?>
