﻿<?php
 include "inc/header.php";


 //Update copyright text
 if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $copytext = mysqli_real_escape_string($db->link, $_POST['copytext']);

    if (empty($copytext)) {
        $copy_msg = "<p style='color:orange'> This field must't be empty</p>";
    }else{
        $update = "UPDATE tbl_co_right SET copytext = '$copytext' WHERE id = '1' ";
        $update_row = $db->update($update);
        if ($update_row) {
            $copy_msg = "<p style='color:green'> Data updated succesfully</p>";
        }else{
           $copy_msg = "<p style='color:orange'> Data not updated</p>"; 
        }
    }
 }
?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Update Copyright Text</h2>
                <div class="block copyblock"> 
                 <form action="" method="POST">
                    <table class="form">
                    <?php 
                    $query = "SELECT * FROM tbl_co_right WHERE id = '1' ";
                    $showData = $db->select($query);
                    if ($showData) {
                        while ( $result = $showData->fetch_assoc()) { ?>
                        <tr>
                            <td>
                                <?php if(isset($copy_msg)){echo $copy_msg;}?>
                                <input type="text" value="<?php echo $result['copytext'];?>" name="copytext" class="large" />
                            </td>
                        </tr>
                        <?php } } ?> 
						
						 <tr> 
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
<?php
 include "inc/footer.php";
?>