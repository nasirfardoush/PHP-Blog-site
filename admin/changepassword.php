﻿<?php
 include "inc/header.php";

 // Change passoward 
 $userid = Session::get('id');

if (isset($userid)) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $take_oldPass = $fm->validation(md5($_POST['take_oldPass']));
    $newPass      = $fm->validation(md5($_POST['newPass']));

    $take_oldPass_ok = mysqli_real_escape_string($db->link,$take_oldPass);
    $newPass_ok = mysqli_real_escape_string($db->link,$newPass);

        if (empty($take_oldPass_ok)) {
            $empty_msg = "<p style='color:red'> Fiield empty not allow !</p>";
        }elseif(empty($newPass_ok)){
            $empty_msg = "<p style='color:red'> Fiield empty not allow !</p>";
        }else{
            $query = "SELECT * FROM tbl_user WHERE id ='$userid' AND pass = '$take_oldPass_ok' ";
            $result = $db->select($query);
            if ($result) {
                $newData = "UPDATE tbl_user SET pass = '$newPass_ok' WHERE id = '$userid' ";
                $update_pass = $db->update($newData);
                if ($update_pass) {
                    $update_msg = "<p style='color:green'> New password succesfully created.</p>";
                }else{
                    $update_msg = "<p style='color:red'> Something worng !</p>";
                }
            } else { 
                $passMsg = "<p style='color:red;'>Old Password Not Matched !</p>";
            }
        }
    }
}
?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Change Password</h2>
                <div class="block">               
                 <form action=" " method="POST">
                    <table class="form">					
                        <tr>
                            <td>
                                <label>Old Password</label>
                            </td>
                            <td>
                                 <?php if(isset($passMsg)){echo $passMsg;}?>
                                <input type="password" placeholder="Enter Old Password..."  name="take_oldPass" class="medium" />
                            </td>
                        </tr>
						 <tr>
                            <td>
                            <?php if(isset($update_msg)){echo $update_msg;}?>
                            <?php if(isset($empty_msg)){echo $empty_msg;}?>
                                <label>New Password</label>
                            </td>
                            <td>
                                <input type="password" placeholder="Enter New Password..." name="newPass" class="medium" />
                            </td>
                        </tr>
						 
						
						 <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
<?php
 include "inc/footer.php";
?>