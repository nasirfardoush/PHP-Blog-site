﻿<?php
 include "inc/header.php";

 //Add social media
 if ($_SERVER['REQUEST_METHOD'] == 'POST') {
     $facebook   = $_POST['facebook'];
     $twitter    = $_POST['twitter'];
     $linkedin   = $_POST['linkedin'];
     $googleplus = $_POST['googleplus'];

if (empty($facebook)) {
    $fa_error ="<p style='color:red'>Facebook field must not be empty ! </p>";
}elseif (empty($twitter)) {
    $tw_error ="<p style='color:red'>Twiter field must not be empty ! </p>";
}elseif (empty($linkedin)) {
    $lin_error ="<p style='color:red'>Linkedin field must not be empty ! </p>";
}elseif (empty($googleplus)) {
    $goo_error ="<p style='color:red'>Googleplus field must not be empty ! </p>";
}else{

    $query = "INSERT INTO tbl_social (facebook,twitter,linkedin,googleplus) VALUES ('$facebook','$twitter','$linkedin','$googleplus')";
    $query = "UPDATE tbl_social  SET 
    facebook   = '$facebook',
    twitter    = '$twitter',
    linkedin   = '$linkedin',
    googleplus = '$googleplus'
    WHERE id = '1' ";
    $updated_link = $db->update($query);
    if ($updated_link) {
        $soc_up_msg ="<p style='color:green'>Data Updated succesfully.</p>";
    }else{
        $soc_up_msg ="<p style='color:orange'>Data not Updated !</p>";
    }
}

 }

    //show social media for editing
    $data = "SELECT * FROM tbl_social WHERE id = '1' ";
    $showdata = $db->select($data);
    if ($showdata) {
        $sociallink = $showdata->fetch_assoc(); 
    }


?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Update Social Media</h2>
                <div class="block">               
                 <form action="" method="POST">
                    <table class="form">
                            <?php if(isset($soc_up_msg)){ echo $soc_up_msg;}?>					
                        <tr>
                            <td>
                                <label>Facebook</label>
                            </td>
                            <td>
                                <?php if(isset($fa_error)){ echo $fa_error;}?>
                                <input type="text" name="facebook" value="<?php echo $sociallink['facebook'];?>" class="medium" />
                            </td>
                        </tr>
						 <tr>
                            <td>
                                <label>Twitter</label>
                            </td>
                            <td>
                                <?php if(isset($tw_error)){ echo $tw_error;}?>
                                <input type="text" name="twitter" value="<?php echo $sociallink['twitter'];?>" class="medium" />
                            </td>
                        </tr>
						
						 <tr>
                            <td>
                                <label>LinkedIn</label>
                            </td>
                            <td>
                                <?php if(isset($lin_error)){ echo $lin_error;}?>
                                <input type="text" name="linkedin" value="<?php echo $sociallink['linkedin'];?>" class="medium" />
                            </td>
                        </tr>
						
						 <tr>
                            <td>
                                <label>Google Plus</label>
                            </td>
                            <td>
                                <?php if(isset($goo_error)){ echo $goo_error;}?>
                                <input type="text" name="googleplus" value="<?php echo $sociallink['googleplus'];?>" class="medium" />
                            </td>
                        </tr>
						
						 <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
<?php
 include "inc/footer.php";
?>
