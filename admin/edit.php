<?php
    include "inc/header.php";
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Post</h2>
                <div class="block">  
<?php 
if(isset($_GET['editId'])){
    $editPostID = $_GET['editId'];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $title      = mysqli_real_escape_string($db->link, $_POST['title']);
    $cat        = mysqli_real_escape_string($db->link, $_POST['cat']);
    $body       = mysqli_real_escape_string($db->link, $_POST['body']);
    $author     = mysqli_real_escape_string($db->link, $_POST['author']);
    $tag        = mysqli_real_escape_string($db->link, $_POST['tag']);

    $alow    = array('jpg','jpeg','png','gif');
    $file_name      = $_FILES['img']['name'];
    $file_size      = $_FILES['img']['size'];
    $file_path      = $_FILES['img']['tmp_name'];


    $explodeEX      = explode('.', $file_name);
    $unique_image   = strtolower(end($explodeEX));
    $file_Extention = substr(md5(time()), 0,7).'.'.$unique_image;

    $file_with_folder ="upload/".$file_Extention;
   

    if(empty($title)){
        $t_error = "<p style='color:red'>Title can't be empty !</p>";
    }elseif (empty($cat)) {
        $c_error = "<p style='color:red'>cat can't be empty !</p>";
    }elseif (empty($body)) {
        $b_error = "<p style='color:red'>body can't be empty !</p>";
    }elseif (empty($file_name)) {
         $i_error = "<p style='color:red'>img can't be empty !</p>";
    }elseif (empty($author)) {
         $a_error = "<p style='color:red'>author can't be empty !</p>";
    }elseif (empty($tag)) {
         $ta_error = "<p style='color:red'>Tag can't be empty !</p>";
    }elseif ($file_size >1048576) {
        echo "<p style='color:red'>file can't be large 1 MB</p>";
    }elseif (in_array($unique_image , $alow )===false) {
        echo "You can upload only".implode(", ", $alow);
    }else{
        move_uploaded_file($file_path , $file_with_folder);
        $query = "UPDATE tbl_post SET
                title    = '$title',
                cat      = '$cat',
                body     = '$body',
                author   = '$author',
                tag      = '$tag',
                img      =  '$file_with_folder'

                WHERE id = '$editPostID' ";
        $update_data = $db->update($query);
        if ($update_data) {
            echo "<p style='color:green'>Data Update succesfully</p>";
        }else{
            echo "<p style='color:red'>Data not Updated</p>";
        }
    }

    }
?>
                <?php 
                    $query = "SELECT * FROM tbl_post WHERE id ='$editPostID' ";
                    $category  =  $db->select($query);
                    if($category){
                    $result = $category->fetch_assoc(); 
                    } ?>
                 <form action="" method="POST" enctype="multipart/form-data">
                    <table class="form">                       
                        <tr>
                            <?php if (isset($t_error)) {
                               echo $t_error;
                            } ?>
                            <td>
                                <label>Title</label>
                            </td>
                            <td>
                                <input type="text" name="title" value="<?php echo $result['title'];?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <?php if (isset($c_error)) {
                               echo $c_error;
                            } ?>
                            <td>
                                <label>Category</label>
                            </td>
                            <td>
                               <select id="select" name="cat">
                                <?php 
                                    $query = "SELECT * FROM tbl_category";
                                    $category  =  $db->select($query);
                                    if($category){
                                        while ($resultCate = $category->fetch_assoc() ) { ?>
                                            <option

                                                <?php
                                                if ($result['cat'] == $resultCate['id']) {?>
                                                    selected = "selected";

                                                <?php } ?> value="<?php echo $resultCate['id'];?>"><?php echo $resultCate['category'];?></option>
                                    <?php }} ?>
                                    
                                </select>
                            </td>
                        </tr>
                   
                    
                        <tr>
                            <?php if (isset($i_error)) {
                               echo $i_error;
                            } ?>

                            <td> 
                                <label>Upload Image</label>
                            </td>
                            <td>
                                <input type="file"  name="img" />

                                 <img src="<?php echo $result['img'];?>" height="40" width="100" >
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Content</label>
                            </td>
                            <td>
                                <textarea class="tinymce" name="body"><?php echo $result['body'];?>
                                    
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <?php if (isset($a_error)) {
                               echo $a_error;
                            } ?>
                            <td>
                                <label>Author</label>
                            </td>
                          <td>
                            <input type="text" name="author" value="<?php echo $result['author'];?>" class="medium" />
                        </td>
                        </tr>
                         <tr>
                            <?php if (isset($ta_error)) {
                               echo $ta_error;
                            } ?>
                            <td>
                                <label>Tag</label>
                            </td>
                          <td>
                            <input type="text" name="tag" value="<?php echo $result['tag'];?>" class="medium" />
                        </td>
                        </tr>
						<tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php
        include "inc/footer.php";
    ?>
