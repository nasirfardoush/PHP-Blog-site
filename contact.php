<?php
	include "inc/header.php";

	//Submit contact information
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		 $first_name = $fm->validation($_POST['first_name']);
		 $last_name  = $fm->validation($_POST['last_name']);
		 $email      = $fm->validation($_POST['email']);
		 $message    = $fm->validation($_POST['message']);

		$ok_first_name = mysqli_real_escape_string($db->link, $first_name);
		$ok_last_name  = mysqli_real_escape_string($db->link, $last_name);
		$ok_email      = mysqli_real_escape_string($db->link, $email);
		$ok_message    = mysqli_real_escape_string($db->link, $message);

		if (empty($ok_first_name)) {
			$f_error = "<p style='color:orange'>Fast name mustn't be empty !";
		}elseif(empty($ok_last_name)){
			$l_error = "<p style='color:orange'>Last name mustn't be empty !";	
		}elseif(empty($ok_email)){
			$e_error = "<p style='color:orange'>Fast name mustn't be empty !";
		}elseif(!filter_var($ok_email, FILTER_VALIDATE_EMAIL)){
			$e_error = "<p style='color:orange'>Invalidate email address !";
		}elseif(empty($ok_message)){
			$msg_error = "<p style='color:orange'>Message mustn't be empty !";
		}else{
			$insert = "INSERT INTO tbl_contact (first_name,last_name,email,message,status) VALUES('$ok_first_name','$ok_last_name','$ok_email','$ok_message','0')";
			$insert_row = $db->insert($insert);
			if ($insert_row) {
				$ins_msg = "<p style='color:green'>Message send succesfully";	
							
			}else{
				$ins_msg = "<p style='color:orange'>Message not sended !";
			}
		}

	}
?>



	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
			<div class="about">
				<h2>Contact us</h2>
			<form action="" method="post">
				<table>
				<?php if(isset($ins_msg)){ echo $ins_msg;}?>
				<tr>
					<td>Your First Name:</td>
					<td>
					<?php if(isset($f_error)){ echo $f_error;}?>
					<input type="text" name="first_name" placeholder="Enter first name"/>
					</td>
				</tr>
				<tr>
					<td>Your Last Name:</td>
					<td>
					<?php if(isset($l_error)){ echo $l_error;}?>
					<input type="text" name="last_name" placeholder="Ente
					r Last name"/>
					</td>
				</tr>
				
				<tr>
					<td>Your Email Address:</td>
					<td>
					<?php if(isset($e_error)){ echo $e_error;}?>
					<input type="email" name="email" placeholder="Enter Email Address"/>
					</td>
				</tr>
				<tr>
					<td>Your Message:</td>
					<td>
					<?php if(isset($msg_error)){ echo $msg_error;}?>
					<textarea name="message" ></textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
					<input type="submit" name="submit" value="SEND"/>
					</td>
				</tr>
		</table>
	<form>				
 </div>

		</div>
		<?php 
			include "inc/sidebar.php";
		?>
	</div>

<?php 
	include "inc/fotter.php";
?>